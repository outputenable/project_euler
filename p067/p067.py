#!/usr/bin/env python3

import functools
import itertools
import operator

def pairwise_max(cols):
    ci = iter(cols)
    a = next(ci)
    yield a
    for b in ci:
        yield max(a, b)
        a = b
    yield a

def path_sum(rows):
    return functools.reduce(
      lambda r, s: map(operator.add, pairwise_max(r), s),
      rows)

def parse_row(line):
    return map(int, line.split())

def rows(lines):
    return (parse_row(line) for line in lines)

if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(
      description='''
        Project Euler Problem 67: Maximum path sum II
        (see https://projecteuler.net/problem=67)''')
    parser.add_argument(
      'file', nargs='?', default='triangle.txt',
      help='Text file containing triangle row data; defaults to triangle.txt')
    parser.add_argument(
      '-n', type=int, help='Number of rows to process (must be positive).')

    args = parser.parse_args()

    with open(args.file) as f:
        print(max(path_sum(itertools.islice(rows(f), args.n))))
