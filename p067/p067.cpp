/*
    Solution to Project Euler problem 67 - Maximum path sum II
    Copyright (C) 2015  Oliver Ebert <oe@outputenable.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <istream>
#include <limits>
#include <ostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

using row = std::vector<int>;

class row_reader
	{
	std::istream *rows_;
	row::size_type n_;
		// row/line number (also the number of columns to read)

 public:
	row_reader(std::istream &rows): rows_(&rows), n_(1) {}
	bool get_row(row &r);
	};

std::string g_progname;

row::value_type max_path_sum(row_reader &rr);

void
print_usage(std::ostream &out)
	{
	out
	  << "Project Euler Problem 67: Maximum path sum II\n"
	     "usage: " << g_progname << " [FILE]\n"
	     "\n"
	     "where\n"
	     "  FILE  text file containing triangle row data;\n"
	     "        defaults to \"triangle.txt\"\n"
	     "\n"
	     "(see https://projecteuler.net/problem=67)"
	  << std::endl;
	}

int
main(int argc, char *argv[])
	{
	g_progname = (argv[0] && *argv[0] != '\0') ? argv[0] : "p067";
	char const *filename;
	if (argc < 2)
		filename = "triangle.txt";
	else if (argc == 2 && *argv[1] != '-')
		filename = argv[1];
	else
		{
		print_usage(std::cerr);
		return EXIT_FAILURE;
		}
	std::ifstream rows(filename);
	if ( !rows.is_open())
		{
		if (argc == 2)
			std::cerr << g_progname << ": Error opening input file: "
			  << filename << '\n';
		else
			print_usage(std::cerr);
		return EXIT_FAILURE;
		}
	row_reader rr(rows);
	std::cout << max_path_sum(rr) << std::endl;
	return EXIT_SUCCESS;
	}

row::value_type
max_path_sum(row_reader &rr)
	{
	auto const min = std::numeric_limits<row::value_type>::min();
	row last(1, min);
	if ( !rr.get_row(last))
		return min;  // or throw?
	assert(last.size() >= 2);
	//
	// last    c(n-1,0)   c(n-1,1)   c(n-1,2) ... c(n-1,n-1)   c(n-1,n)
	//           =min :   /      \max/      \ max /        \   : =min
	//                 :+/        \+/        \ + /          \+:
	// curr  c(n,0)   c(n,1)     c(n,2)       ...          c(n,n)
	//        =min
	//
	for (row curr(1, min); rr.get_row(curr); last.swap(curr), curr.resize(1))
		{
		assert(curr.size() == last.size() + 1);
		last.push_back(min);
		auto i = ++curr.begin();
		auto ii = last.cbegin();
		auto const e = curr.end();
		do
			// TODO - check for arithmetic overflow!?
			*i += std::max(*ii, *(ii + 1));
			while (++ii, ++i != e);
		}
	return *std::max_element(++last.cbegin(), last.cend());
	}

bool
row_reader::get_row(row &r)
	{
	assert(rows_->good());

	if (n_ == 0)
		throw std::runtime_error("Too many rows/columns");
	std::istringstream iss;
	{
		std::string line;
		std::getline(*rows_, line);
		if ( !line.empty())
			iss.str(line);
		else
			return false;
		}
	row::value_type i;
	row::size_type cols = n_;
	for (;;)
		{
		if (iss >> i)
			{
			if (cols != 0)
				{
				--cols;
				r.push_back(i);
				continue;
				}
			std::ostringstream what;
			what << "Too many columns on row/line " << n_;
			throw std::runtime_error(what.str());
			}
		if (cols == 0)
			{
			++n_;
			return true;
			}
		std::ostringstream what;
		what << "Missing or malformed column(s) on row/line " << n_;
		throw std::runtime_error(what.str());
		}
	}
